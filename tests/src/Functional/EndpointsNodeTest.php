<?php

namespace Drupal\Tests\gatsby_endpoints\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\gatsby_endpoints\Entity\GatsbyEndpoint;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Defines a test for the creation of nodes with endpoints enabled.
 *
 * @group gatsby_endpoints
 *
 * @requires module jsonapi_extras
 */
class EndpointsNodeTest extends BrowserTestBase {

  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   *
   * @todo Remove this in https://www.drupal.org/project/gatsby/issues/3198673
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to bypass content access checks.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'gatsby_endpoints',
    'jsonapi_extras',
    'path_alias',
    'node',
    'field',
  ];

  /**
   * Tests the Endpoint creation.
   */
  public function testEndpointsNodeCreation() {
    $node_type = $this->drupalCreateContentType([
      'type' => 'article',
    ]);
    $node_type->setThirdPartySetting('gatsby', 'iframe', TRUE);
    $node_type->setThirdPartySetting('gatsby', 'preview', TRUE);
    $node_type->setThirdPartySetting('gatsby', 'target', 'window');
    $node_type->save();

    $this->drupalLogin($this->drupalCreateUser([
      'access content',
      'edit any article content',
    ]));
    FieldStorageConfig::create([
      'field_name' => 'endpoint',
      'entity_type' => 'node',
      'type' => 'gatsby_endpoint_reference',
    ])->save();

    FieldConfig::create([
      'field_name' => 'endpoint',
      'entity_type' => 'node',
      'type' => 'gatsby_endpoint_reference',
      'bundle' => 'article',
    ])->save();

    $endpoint_id = $this->randomMachineName();
    $endpoint = GatsbyEndpoint::create([
      'id' => $endpoint_id,
      'plugin' => 'jsonapi',
      'preview_urls' => [
        'preview_url' => ['http://example.com'],
      ],
      'build_urls' => [
        'build_url' => ['http://example.com'],
      ],
      'settings' => [],
      'build_entity_types' => [
        [
          'entity_type' => 'node',
          'entity_bundles' => ['article' => 'article'],
          'include_entities' => ['node' => 'node'],
        ],
      ],
      'included_entity_types' => [],
    ]);
    $endpoint->save();

    $article = $this->drupalCreateNode([
      'type' => 'article',
      'title' => $this->randomMachineName(),
      'status' => 1,
      'endpoint' => ['target_id' => $endpoint_id],
    ]);
    $assert = $this->assertSession();
    $this->drupalGet($article->toUrl('edit-form'));
    $assert->statusCodeEquals(200);
    $assert->buttonExists('Open Gatsby Preview');

    $this->drupalGet($article->toUrl());
    $assert->statusCodeEquals(200);
    $assert->elementExists('css', '.gatsby-iframe-container');
  }

}
